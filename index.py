

def findWaitingTime(n, bt, wt):

    # waiting time for
    # first process is 0
    wt[0] = 0

    # calculating waiting time
    for i in range(1, n):
        wt[i] = bt[i - 1] + wt[i - 1]


def findTurnAroundTime(n, bt, wt, tat):

    # calculating turnaround
    # time by adding bt[i] + wt[i]
    for i in range(n):
        tat[i] = bt[i] + wt[i]


def findavgTime(processes, n, bt):

    wt = [0] * n  # waiting time
    tat = [0] * n  # turn around time
    total_wt = 0  # total waiting time
    total_tat = 0  # total turn around time

    # Function to find waiting
    # time of all processes
    findWaitingTime(n, bt, wt)

    # Function to find turn around
    # time for all processes
    findTurnAroundTime(n, bt, wt, tat)

    # Display processes along
    # with all details
    print("Processes Burst time " +
          " Waiting time " +
          " Turn around time")

    # Calculate total waiting time
    # and total turn around time
    for i in range(n):

        total_wt = total_wt + wt[i]
        total_tat = total_tat + tat[i]
        print(" " + str(i + 1) + "\t\t" +
              str(bt[i]) + "\t " +
              str(wt[i]) + "\t\t " +
              str(tat[i]))

    print("Average waiting time = " +
          str(total_wt / n))
    print("Average turn around time = " +
          str(total_tat / n))


# Driver code
if __name__ == "__main__":

    processes_array = [1, 2, 3]  # fix process!!!!
    processes_len = len(processes_array)

    # Burst time of all processes
    burst_time_array = list()
    for i in range(int(processes_len)):
        n = input("burst_time of process %d: " % (processes_array[i]))
        burst_time_array.append(int(n))

    findavgTime(processes_array, processes_len, burst_time_array)
